package com.liuzy.androidhttps;

import java.security.KeyStore;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import com.liuzy.tool.HTTPS;
import com.liuzy.tool.HttpClientTool;
import com.liuzy.tool.KsManager;

public class MainActivity extends Activity {
	private static final String HTTPS_URL = "https://test.liuzy.xyz";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		new AsyncTask<Object, Object, Object>() {
			@Override
			protected String doInBackground(Object... arg0) {
				Context ctx = MainActivity.this.getBaseContext();
				
				// -----------------第一种------------------
				// 把客户端证书和私钥用openssl导出为P12格式
				String p12File = "client.p12";
				// 你需要知道P12的密码
				String pwd1 = "123456";
				// 生成KeyStore，密码为P12的密码
				KeyStore keyStore1 = KsManager.getKeyStoreByP12(ctx, p12File, pwd1);
				
				// 把服务器证书用keytool工具生成KeyStore并导入其中
				String bksFile = "client.bks";
				// 你需要知道bks的密码
				String bksPwd = "123456";
				// 生成TrustStore
				KeyStore trustStore1 = KsManager.getTrustStoreByBks(ctx, bksFile, bksPwd);
				// ---------------------------------------

				// -----------------第二种-------------------
				// 生成你的私钥文件，内容是...BEGIN RSA PRIVATE KEY...
				String clientPem = "client.pem";
				// 根据你的私钥生成客户端证书
				String clientCrt = "client.crt";
				// 为KeyStore设置一个密码
				String pwd2 = "123456";
				// 生成KeyStore
				KeyStore keyStore2 = KsManager.getKeyStoreByCrtPem(ctx, clientCrt, clientPem, pwd2);

				// 服务器证书
				String serverCrt = "server.crt";
				// 生成TrustStore
				KeyStore trustStore2 = KsManager.getTrustStoreByCrt(ctx, serverCrt);
				// ---------------------------------------

				// -------------使用HTTPS工具类---------------
				// 初始化方法只需要调用一次
				HTTPS.init(keyStore2, pwd1, trustStore2);
				// 发起请求
				new HTTPS().doGET(HTTPS_URL, null);

				// ----------使用HttpClientTool工具类----------
				// 初始化方法只需要调用一次
				HttpClientTool.init(keyStore1, pwd2, trustStore1);
				// 发起请求
				HttpClientTool.doGET(HTTPS_URL, null);

				return null;
			}
		}.execute();
	}

}
